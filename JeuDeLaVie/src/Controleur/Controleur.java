package Controleur;

import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.Timer;
import javax.swing.*;

import Modele.*;
import Vue.*;


/**
 * La classe Controleur permet de synchroniser la vue et le mod�le, en fonction des actions effectu�es par la vue.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class Controleur implements ActionListener {

	Damier chDamier;
	
	PanelNord chPanelNord;
	PanelCentre chPanelCentre;
	PanelSud chPanelSud;
	PanelJeu chPanelJeu;
	
	Timer chTimer = new Timer();
	ArrayList <Damier> chListeDamiers;
	ArrayList <String> chNomsDamiers;
	ArrayList <JMenuItem> chListeJMenuItem;
	JMenu chMenuExemples;
	
	
	/**
	 * Constructeur du controleur
	 * @param parDamier Le damier actuel.
	 * @param parPN Le PanelNord.
	 * @param parPC Le PanelCentre.
	 * @param parPS Le PanelSud.
	 * @param parPJ Le PanelJeu.
	 * @param parNomsDamiers La liste des noms des damiers exemples.
	 * @param parListeDamiers La liste des damiers exemples.
	 * @param parListeJMenuItem La list des JMenuItem des damiers exemples.
	 * @param parMenuExemples Le JMenu Menu exemples.
	 */
	public Controleur(Damier parDamier, PanelNord parPN, PanelCentre parPC, PanelSud parPS, PanelJeu parPJ, ArrayList <String> parNomsDamiers,
			ArrayList <Damier> parListeDamiers, ArrayList <JMenuItem> parListeJMenuItem , JMenu parMenuExemples) {
		
		chDamier = parDamier;
		chPanelNord = parPN;
		chPanelCentre = parPC;
		chPanelSud = parPS;
		chPanelJeu = parPJ;
		
		chNomsDamiers = parNomsDamiers;
		chListeDamiers = parListeDamiers;
		chListeJMenuItem = parListeJMenuItem;
		chMenuExemples = parMenuExemples;
		
		chPanelCentre.enregistreEcouteur(this);
		chPanelSud.enregistreEcouteur(this);
		chPanelJeu.enregistreEcouteur(this);
		
	}

	/**
	 * Permet de g�rer les diff�renst �v�n�ments, afin de modifier le modele et la vue en cons�quence.
	 */
	public void actionPerformed(ActionEvent parEvt) {
			
		
		//***** INSTANCE OF BOUTONCELLULE *****//
		
		if (parEvt.getSource() instanceof BoutonCellule) {
			BoutonCellule chBouton = (BoutonCellule) parEvt.getSource();
			chDamier.inverseEtat(chBouton.getLigne(), chBouton.getColonne());
			chBouton.inverseEtat();
			chBouton.setCouleur();
			chPanelNord.setPop(chDamier.getPopulation());
		}
		
		
		//***** ALEATOIRES *****//
		
		for (int i=0; i<Data.DAM_ALEA.length; i++) {
			if (parEvt.getActionCommand().equals(Data.DAM_ALEA[i])){
				chTimer.cancel();
				int chTaille = Data.TAILLE[i];
				chDamier.damierAlea(chTaille, chTaille);
				chPanelCentre.changementDamier(chDamier);
				chPanelCentre.enregistreEcouteur(this);
				chPanelNord.setPop(chDamier.getPopulation());
				chPanelNord.setGen(1);
				break;
			}
		}
		
		
		//***** VIDES *****//
		
		for (int i=0; i<Data.DAM_VIDES.length; i++) {
			if (parEvt.getActionCommand().equals(Data.DAM_VIDES[i])){
				chTimer.cancel();
				int chTaille = Data.TAILLE[i];
				chDamier.damierVide(chTaille, chTaille);
				chPanelCentre.changementDamier(chDamier);
				chPanelCentre.enregistreEcouteur(this);
				chPanelNord.setPop(chDamier.getPopulation());
				chPanelNord.setGen(1);
				break;
			}
		}
		
		
		//***** GENERATION SUIVANTE *****//
		
		if (parEvt.getActionCommand().equals(Data.BOUTONS_SUD[0])) {
			if(chDamier.getPopulation()==0) {
				popNulle();
			}
			else {
				chDamier.nextGen();
				chPanelCentre.updatePanel();
				chPanelNord.setPop(chDamier.getPopulation());
				chPanelNord.setGen(chDamier.getGeneration());
			}
		}
		
		
		//***** AVANCE RAPIDE *****//
		
		if (parEvt.getActionCommand().equals(Data.BOUTONS_SUD[1])) {
			chTimer.cancel();
			
			chTimer = new Timer(); 
			int chPeriode = 100; //Temps en ms
			chTimer.scheduleAtFixedRate(new TimerTask() {
				public void run()  { 
					if (chDamier.getPopulation()==0) {
						chTimer.cancel();
						popNulle();
					}
					else {
						chDamier.nextGen();
						chPanelCentre.updatePanel();
						chPanelNord.setPop(chDamier.getPopulation());
						chPanelNord.setGen(chDamier.getGeneration());
					}
				}
			}, 0, chPeriode);
		}
		
		
		//***** PAUSE *****//
		
		if (parEvt.getActionCommand().equals(Data.BOUTONS_SUD[2])) {
			if (chTimer !=null) {
				chTimer.cancel();
			}
			
		}
		
		
		//***** ENREGISTRER *****//

		if (parEvt.getActionCommand().equals(Data.BOUTONS_SUD[3])) {
			String chNomDamier = chPanelSud.getNomNewDamier();
			
			if (chNomDamier.isEmpty()) {
				JFrame frame = new JFrame();
				String message = "<html><body><div width='200px' align='center'>Vous n'avez pas donn� de nom � votre damier !</div></body></html>";
				JOptionPane.showMessageDialog(frame, message);
			}
			else {
				
				if ( chNomsDamiers.contains(chNomDamier) ) {
					for (int i=0; i < chNomsDamiers.size(); i++) {
						if (chNomsDamiers.get(i).equals(chNomDamier)) {
							chNomsDamiers.remove(i);
							chListeDamiers.remove(i);
							
							JFrame frame = new JFrame();
							String message = "<html><body><div width='200px' align='center'>L'ancien damier du m�me nom a �t� �cras� !</div></body></html>";
							JOptionPane.showMessageDialog(frame, message);
							break;
						}
					}
				}
				chDamier.setNomDamier(chNomDamier);				
				chListeDamiers.add(chDamier);	
				LectureEcriture.ecriture(new File("damiers"+File.separator+"damiers.ser"), chListeDamiers);
				chPanelSud.resetLabel();
				
				chMenuExemples.removeAll();
				chMenuExemples.validate();
				chMenuExemples.setMnemonic(Data.MENUS[1].charAt(2));

				ArrayList <String> chListeNoms = new ArrayList<String>();
				
				for (int i=0; i < chListeDamiers.size(); i++) {
					Damier chDamier = chListeDamiers.get(i);
					String chNom = chDamier.getNomDamier();
					JMenuItem chMenuItem = new JMenuItem(chNom);
					chMenuItem.setActionCommand(chNom);
					chMenuItem.addActionListener(this);
					chMenuExemples.add(chMenuItem);
					chListeJMenuItem.add(chMenuItem);
					chListeNoms.add(chNom);
				}
				chNomsDamiers = chListeNoms;
				chMenuExemples.revalidate();
				
				JFrame frame = new JFrame();
				String message = "<html><body><div width='200px' align='center'>Votre damier a bien �t� enregistr� !</div></body></html>";
				JOptionPane.showMessageDialog(frame, message);
			}		
		}
			

		//***** EXEMPLES *****//

		for(int i=0; i<chNomsDamiers.size(); i++) {

			File chFileDamiers = new File("damiers"+File.separator+"damiers.ser");
			if (chFileDamiers.length() !=0) {
				chListeDamiers = (ArrayList <Damier>) LectureEcriture.lecture(new File("damiers"+File.separator+"damiers.ser"));
			}
			if (parEvt.getActionCommand().equals(chNomsDamiers.get(i))){
				if (chTimer != null)
					chTimer.cancel();

				chDamier = chListeDamiers.get(i);
				chDamier.resetGen();
				chPanelCentre.changementDamier(chDamier);
				chPanelCentre.enregistreEcouteur(this);
				chPanelNord.setPop(chDamier.getNouvellePop());
				chPanelNord.setGen(chDamier.getGeneration());
				break;
			}
		}
	}
		
	
	/**
	 * Permet d'afficher une message de dialogue lorsque la population est nulle.
	 */
	public void popNulle() {
		chTimer.cancel();
		JFrame frame = new JFrame();
		String message = "<html><body><div width='200px' align='center'>Impossible de continuer, toute la population est morte."
				+ "<br>Rajoutez des cellules ou quittez la partie.</div></body></html>";
		JOptionPane.showMessageDialog(frame, message);
	}
}