package Modele;

/**
 * Classe contenant toutes les constantes utilisées dans les différentes classes du jeu de la vie.
 * 
 * @author A. MARTIN T. ROGLIN
 *
 */
public class Data {

	/**
	 * Nom du jeu.
	 */
	public static final String NOM_JEU = "Jeu de la vie";

	/**
	 * Noms des menus de la JMenuBar.
	 */
	public static final String MENUS [] = {"Damiers aléatoires", "Damiers exemples", "Damiers vides", "Quitter"};

	/**
	 * Noms des JMenuItems du JMenu Damiers vides.
	 */
	public static final String DAM_VIDES [] = {"Vide : 25*25", "Vide : 35*35", "Vide : 45*45", "Vide : 100*100"};

	/**
	 * Noms des JMenuItems du JMenu Damiers aléatoires.
	 */
	public static final String DAM_ALEA [] = {"Aleatoire : 25*25", "Aleatoire : 35*35", "Aleatoire : 45*45","Aleatoire : 100*100"};
	
	
	/**
	 * Noms des JLabels du PanelNord.
	 */
	public static final String INFOS_NORD [] = {"Génération", "Population"};
	
	/**
	 * Noms des boutons du PanelSud.
	 */
	public static final String BOUTONS_SUD [] = {"Génération suivante", "Avance rapide", "Pause", "Enregistrer"};

	/**
	 * Nom du JLabel du PanelSud.
	 */
	public static final String LABEL_SUD = "Nom du damier";
	
	/**
	 * Tailles de damier.
	 */
	public static final int[] TAILLE = {25, 35, 45,100};

}