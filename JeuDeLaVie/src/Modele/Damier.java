package Modele;

import java.io.Serializable;
import java.util.Random;


/**
 * 
 * Cette classe permet de cr��er un damier, qui est un tableau de booleen.
 * Elle contient les m�thodes permettant de changer de g�n�ration ou de g�n�rer un damier vide, al�atoire, ou pr�-enregistr�.
 * Si une valeur du tableau est � true, la cellule associ�e est en vie, sinon elle est morte.
 *
 * @author A. MARTIN T. ROGLIN
 *
 */
public class Damier implements Serializable {

	private boolean chEtatCellule [][];
	private static int chPopulation = 0;
	private static int chGeneration = 1;
	private  String chNom ;


	/**
	 * Constructeur par d�faut, lorsque aucun parametre n'est entr� pour la cr�ation du Damier.
	 */
	public Damier() {
		chEtatCellule = new boolean[25][25];
	}
	
	
	/**
	 * Constructeur prenant en entr�e un tableau de bool�ens.
	 * @param parBool Un tableau de bool�ens.
	 */
	public Damier(boolean[][] parBool) {
		chEtatCellule = parBool;
		for (int i=0; i< parBool.length; i++) {
			for (int j=0; j<parBool[i].length; j++)
				if (parBool[i][j])
					chPopulation++;
		}
	}
	
	
	/**
	 * M�thode permettant de g�n�rer un damier al�atoire.
	 * @param parL Int : nombre de lignes;
	 * @param parC Int : nombre de colonnes;
	 */
	public void damierAlea(int parL, int parC){
		boolean[][] chDamRandom = new boolean[parL][parC];
		chPopulation = 0;
		Random random = new Random();
		for (int i=0; i< chDamRandom.length; i++)
			for (int j=0; j< chDamRandom[i].length; j++) {
				chDamRandom[i][j] = random.nextBoolean();
				if( chDamRandom[i][j])
					chPopulation++;
			}
		chGeneration = 1;
		chEtatCellule = chDamRandom;
	}
	
	
	/**
	 * M�thode permettant de g�n�rer un damier vide.
	 * @param parL Int : nombre de lignes;
	 * @param parC Int : nombre de colonnes;
	 */
	public void damierVide(int parL, int parC){
		boolean[][] chDamier = new boolean[parL][parC];
		chEtatCellule = chDamier;
		chPopulation = 0;
		chGeneration = 1;
	}
	
	
	/**
	 * M�thode permettant de retourner le tableau dans la console.
	 * X pour une cellule morte
	 * O pour une cellule en vie.
	 */
	public String toString() {
		String string = "";
		for (int i=0; i< chEtatCellule.length; i++) {
			
			for (int j=0; j< chEtatCellule[i].length; j++) {
				if (chEtatCellule[i][j]) {
					string +="O ";
				}
				else
					string += "X "; 
			}
			string += "\n";
		}
		return string;
	}
	
	
	/**
	 * M�thode permettant de calculer le nombre de voisins en vie d'une cellule.
	 * @param parL Int : num�ro de ligne.
	 * @param parC Int : num�ro de colonne.
	 * @return Int : le nombre de voisins d'une cellule(parL, parC) du jeu de la vie.
	 */
	public int nbVoisinsEnVie (int parL, int parC) {
		int compteur = 0;
		
		if(parL !=0 && parC !=0)
			if (getEtat(parL-1, parC-1))
				compteur++;
		
		if(parL != getNbLignes()-1 && parC != getNbColonnes()-1)
			if (getEtat(parL+1, parC+1))
				compteur++;
		
		if(parC !=0)
			if (getEtat(parL, parC-1))
				compteur++;
		
		if(parL !=0)
			if (getEtat(parL-1, parC))
				compteur++;
		
		if(parL != getNbLignes()-1 && parC !=0)
			if (getEtat(parL+1, parC-1))
				compteur++;
		
		if(parL !=0 && parC !=getNbColonnes()-1)
			if (getEtat(parL-1, parC+1))
				compteur++;
		
		if(parL != getNbLignes()-1)
			if (getEtat(parL+1, parC))
				compteur++;
		
		if(parC != getNbColonnes()-1)
			if (getEtat(parL, parC+1))
				compteur++;
			
		return compteur;
	}
	
	
	/**
	 * M�thode permettant de mettre � jour le damier avec la g�n�ration suivante.
	 * On utilise donc la m�thode nbVoisinsEnVie, pour connaitre le futur etat d'une cellule.
	 * Cette m�thode met �galement � jour la g�n�ration et la population du jeu de la vie.
	 */
	public void nextGen() {
		boolean[][] nextGen = new boolean [getNbLignes()][getNbColonnes()];
		chPopulation = 0;
		for (int i=0; i<getNbLignes(); i++) {
			for (int j=0; j<getNbColonnes(); j++) {
				if (!(getEtat(i,j)) && (nbVoisinsEnVie(i,j)==3)){
					nextGen[i][j] = true;
					chPopulation++;
				}

				else if ((getEtat(i,j)) && (nbVoisinsEnVie(i,j)==2 || nbVoisinsEnVie(i,j)==3)){
					nextGen[i][j] = true;
					chPopulation++;
				}
			}
		}	
		chEtatCellule = nextGen;
		chGeneration++;
	}
	
	
	/**
	 * M�thode permettant de connaitre la population, lorsqu'on utilise la fonction "g�n�ration suivante".
	 * @return chPopulation : la taille de la population.
	 */
	public int getPopulation() {
		return chPopulation;
	}
	
	
	/**
	 * M�thode permettant de connaitre la population lorsque l'on vient de cr�er un nouveau damier et donc un nouveau jeu de la vie.
	 * @return chPopulation : la taille de la population.
	 */
	public int getNouvellePop() {
		chPopulation = 0;
		for (int i=0; i<getNbLignes(); i++) {
			for (int j=0; j<getNbColonnes(); j++) {
				if (chEtatCellule[i][j]){
					chPopulation++;
				}
			}
		}
		return chPopulation;
	}

	
	/**
	 * M�thode permettant de connaitre la g�n�ration du jeu actuel.
	 * @return chGeneration : le num�ro de g�n�ration.
	 */
	public int getGeneration() {
		return chGeneration;
	}
	
	
	/**
	 *  M�thode permettant de mettre � jour la g�n�ration.
	 * @param parGen Un num�ro de g�n�ration.
	 */
	
	public void resetGen() {
		chGeneration = 1;
	}
	
	
	/**
	 * M�thode permettant de connaitre l'�tat d'une cellule, en fonction de ses coordonn�es dans le damier.
	 * @param parL Num�ro de ligne.
	 * @param parC Num�ro de colonne.
	 * @return Bool�en : �tat de la cellule.
	 */
	public boolean getEtat(int parL, int parC) {
		return chEtatCellule[parL][parC];
	}
	
	
	/**
	 * M�thode permettant changer l'�tat d'une cellule".
	 * @param parL Num�ro de ligne.
	 * @param parC Num�ro de colonne.
	 */
	public void inverseEtat(int parL, int parC) {
		   chEtatCellule[parL][parC] = !chEtatCellule[parL][parC];
		   if(chEtatCellule[parL][parC]) {
			   chPopulation++;
		   }
		   else {
			   chPopulation--;
		   }
	    }
	
	
	/**
	 * M�thode permettant de connaitre le nombre de lignes d'un damier.
	 * @return Int : nombre de lignes d'un damier.
	 */
	public int getNbLignes() {
		return chEtatCellule.length;
	}
	
	
	/**
	 * M�thode permettant de connaitre le nombre de colonnes d'un damier.
	 * @return Int : nombre de colonnes d'un damier.
	 */
	public int getNbColonnes() {
		return chEtatCellule[0].length;
	}
	

	/**
	 * Permet de donner un nom au Damier actuel.
	 * @param parNom Le nom que l'on veut donner au Damier..
	 */
	public void setNomDamier(String parNom) {
		chNom = parNom;
	}
	
	
	/**
	 * Permet de r�cup�rer le nom du damier actuel.
	 * @return Le nom du damier actuel.
	 */
	public String getNomDamier() {
		return chNom;
	}
	
}
