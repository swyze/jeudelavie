package Modele;


import java.io.*;


/**
 * Cette classe abstraite permet d'�crire et de lire dans des fichiers.
 * 
 * @author A. MARTIN T. ROGLIN
 *
 */
public class LectureEcriture {
	
	
	/**
	 * Cette m�thode permet de lire des fichier.
	 * @param parFichier L'adresse du fichier � lire.
	 * @return Le contenu d'un fichier sous la forme d'un objet.
	 */
	public static Object lecture (File parFichier){
		ObjectInputStream flux;
		Object objetLu = null;
		

		try {
			flux = new ObjectInputStream(new FileInputStream(parFichier));
			objetLu = (Object)flux.readObject();
			flux.close();
		} catch (ClassNotFoundException e) {
			System.err.println(e.toString());
			System.exit(-1);
			
		} catch (IOException e) {
			
			System.err.println("Erreur de la lecture du fichier "+e.toString());
			System.exit(-1);		
			}

		return objetLu;
	
	}
	
	/**
	 * Cette m�thode permet d'�crire dans un fichier.
	 * @param parFichier L'adresse du fichier � modifier.
	 * @param parObject L'objet que l'on veut sauvegarder dans le fichier.
	 */
	public static void ecriture (File parFichier, Object parObject){
		ObjectOutputStream flux;
		
		try {
			flux = new ObjectOutputStream(new FileOutputStream(parFichier));
			flux.writeObject (parObject);
			flux.flush();
			flux.close();
			} 
		catch (IOException e) {
			System.err.println("Erreur a l'ecriture du fichier  "+e.toString());
			System.exit(-1);
		}
		
	}
	
	
	
}
