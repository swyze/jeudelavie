package Vue;
import java.awt.Color;
import javax.swing.JButton;

/**
 * Classe permettant la creation d'un Bouton contentant l'�tat d'une cellule du damier.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class BoutonCellule extends JButton{
	
	private boolean chEtat;
	private int chLigne;
	private int chColonne;
	
	/**
	 * Constructeur d'un BoutonCellule.
	 * @param parLigne Un num�ro de ligne.
	 * @param parColonne Un num�ro de colonne.
	 * @param parEtat Un boolean d�crivant l'�tat d'une cellule.
	 */
	public BoutonCellule(int parLigne, int parColonne, boolean parEtat) {
		chLigne = parLigne;
		chColonne = parColonne;
		chEtat = parEtat;
	}
	
	
	/**
	 * Renvoie l'�tat d'une cellule.
	 * @return Booleen indiquant l'�tat d'une cellule. true pour une cellule vivante, false pour une cellule morte.
	 */
	public boolean getEtat() {
		return chEtat;
	}
	
	
	/** 
	 * Renvoie la ligne d'une cellule.
	 * @return Ligne d'une cellule dans le damier.
	 */
	public int getLigne() {
		return chLigne;
	}
	
	
	/** 
	 * Renvoie la colonne d'une cellule.
	 * @return Colonne d'une cellule dans le damier.
	 */
	public int getColonne() {
		return chColonne;
	}
	
	
	/**
	 * Permet de mettre � jour la couleur d'une cellule selon son �tat.
	 */
	public void setCouleur() {
		if (chEtat)
			setBackground(Color.BLACK);
		else
			setBackground(Color.WHITE);
	}
	
	
	/**
	 * Permet d'inverser l'�tat d'une cellule.
	 */
	public void inverseEtat() {
		chEtat = !chEtat;
	}
	
	
	/**
	 * Permet de modifier l'�tat d'une cellule (en vie / morte).
	 * @param parEtat Boolen indiquant l'�tat que l'on veut appliquer � la cellule.
	 */
	public void setEtat(boolean parEtat) {
		chEtat = parEtat;
	}
	
}
