package Vue;

import java.awt.Color;
import javax.swing.*;
import javax.swing.border.*;

import Modele.Damier;
import Modele.Data;

/**
 * Cette classe permet l'affichage de la g�n�ration et de la population du jeu de la vie.
 * @author A. MARTIN T. ROGLIN
 */
public class PanelNord extends JPanel {

	private JLabel chGen = new JLabel();
	private JLabel chPop = new JLabel();
	private Damier chDamier;
	
	/**
	 * Constructeur de PanelNord, qui instancie la g�n�ration et la population du jeu de la vie en cours.
	 * @param parDamier Un damier.
	 */
	public PanelNord(Damier parDamier) {
		
		setOpaque(false);

		chDamier = parDamier;
	
		CompoundBorder chCompound = new CompoundBorder(new LineBorder(Color.GRAY, 1, true), new EmptyBorder(10, 10, 10, 10));
		
		chGen.setText(Data.INFOS_NORD[0]+" : "+chDamier.getGeneration());
		chGen.setBorder(chCompound);
		chGen.setOpaque(true);
		
		chPop.setText(Data.INFOS_NORD[1]+" : "+chDamier.getPopulation());
		chPop.setBorder(chCompound);
		chPop.setOpaque(true);


		add(chGen);
		add(chPop);

	}
	
	
	/**
	 * M�thode permettant de mettre � jour le JLabel contenant la g�n�ration du jeu en cours.
	 * @param parGen Int : num�ro de g�n�ration.
	 */
	public void setGen(int parGen) {
		chGen.setText(Data.INFOS_NORD[0]+" : "+Integer.toString(parGen));
	}
	
	
	/**
	 * M�thode permettant de mettre � jour le JLabel contenant la taille de la population du du jeu en cours.
	 * @param parPop Int : taille de la population.
	 */
	public void setPop(int parPop) {
		chPop.setText(Data.INFOS_NORD[1]+" : "+Integer.toString(parPop));
	}
	
}