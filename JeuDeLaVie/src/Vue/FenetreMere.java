package Vue;

import java.awt.Color;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;

import Modele.*;

/**
 * Fenetre mere du jeu contentant le PanelJeu.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class FenetreMere extends JFrame implements ActionListener {

	
	private ArrayList <JMenuItem> chListeJMenuItem = new ArrayList<JMenuItem>();

	
	/**
	 * Constructeur de FenetreMere
	 * @param parTitre Le titre de la fenetre.
	 */
	public FenetreMere(String parTitre) {
		super(parTitre);
		setLocation(490,0);
		setSize(1000,1000);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		//***** JMENUBAR *****//
		JMenuBar chMenuBar = new JMenuBar();
		chMenuBar.setBackground(new Color(232, 232, 232));
		setJMenuBar(chMenuBar);
		
		
		//***** ALEATOIRES *****//
		JMenu chMenuAleatoires = new JMenu(Data.MENUS[0]);
		chMenuAleatoires.setMnemonic(Data.MENUS[0].charAt(0));
		
		for (int i=0; i < Data.DAM_ALEA.length; i++) {
			JMenuItem chMenuItem = new JMenuItem(Data.DAM_ALEA[i]);
			chMenuItem.setActionCommand(Data.DAM_ALEA[i]);
			chMenuAleatoires.add(chMenuItem);
			chListeJMenuItem.add(chMenuItem);
		}
		
		
		//***** EXEMPLES *****// 
		JMenu chMenuExemples = new JMenu(Data.MENUS[1]);
		chMenuExemples.setMnemonic(Data.MENUS[1].charAt(2));

		ArrayList <String> chListeNoms = new ArrayList<String>();
		ArrayList <Damier> chListeDamier = new ArrayList<Damier>();
 
		File chFile = new File("damiers"+File.separator+"damiers.ser");
		if (chFile.length()!=0) {
			chListeDamier = (ArrayList<Damier>) LectureEcriture.lecture(chFile);
		}
		
		for (int i=0; i < chListeDamier.size(); i++) {
			Damier chDamier = chListeDamier.get(i);
			String chNom = chDamier.getNomDamier();
			JMenuItem chMenuItem = new JMenuItem(chNom);
			chMenuItem.setActionCommand(chNom);
			chMenuExemples.add(chMenuItem);
			chListeJMenuItem.add(chMenuItem);
			chListeNoms.add(chNom);
		}
		
		
		//***** VIDES *****//
		JMenu chMenuVides = new JMenu(Data.MENUS[2]);
		chMenuVides.setMnemonic(Data.MENUS[2].charAt(8));

		for (int i=0; i < Data.DAM_VIDES.length; i++) {
			JMenuItem chMenuItem = new JMenuItem(Data.DAM_VIDES[i]);
			chMenuItem.setActionCommand(Data.DAM_VIDES[i]);
			chMenuVides.add(chMenuItem);
			chListeJMenuItem.add(chMenuItem);
		}

	
		//***** QUITTER *****//
		JMenuItem chQuitter = new JMenuItem(Data.MENUS[3]);
		chQuitter.setMnemonic(Data.MENUS[2].charAt(0));
		chQuitter.setAccelerator(KeyStroke.getKeyStroke(Data.MENUS[3].charAt(0),java.awt.Event.CTRL_MASK));
		chQuitter.setActionCommand(Data.MENUS[Data.MENUS.length-1]);
		chQuitter.addActionListener(this);
		chQuitter.setBackground(new Color(255, 196, 196));
		
		
		//***** AJOUTS *****//
		chMenuBar.add(chMenuAleatoires);
		chMenuBar.add(chMenuExemples);
		chMenuBar.add(chMenuVides);
		chMenuBar.add(Box.createHorizontalGlue());
		chMenuBar.add(chQuitter);
		
		PanelJeu contentPane = new PanelJeu(chListeJMenuItem, chMenuExemples, chListeNoms, chListeDamier);
		setContentPane(contentPane);
		setVisible(true);

	}
	
	
	public static void main(String [] args) {
		new FenetreMere(Data.NOM_JEU);
	}


	public void actionPerformed(ActionEvent parEvt) {
		if (parEvt.getActionCommand().equals(Data.MENUS[Data.MENUS.length-1])){
			int quitter = JOptionPane.showConfirmDialog(this, "�tes-vous certain de vouloir quitter ?","Dialogue de confirmation",
					JOptionPane.YES_NO_OPTION);
			if (quitter == JOptionPane.YES_OPTION){
				System.exit(0);
			}	
		}
	}
}