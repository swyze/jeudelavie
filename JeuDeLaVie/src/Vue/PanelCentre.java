package Vue;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

import Controleur.*;
import Modele.*;

/**
 * Classe contenant le damier, permettant d'afficher le jeu de la vie.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class PanelCentre extends JPanel {

	/**
	 * Tableau de BoutonCellule.
	 */
	private BoutonCellule chListeBouton [][] ;
	
	/**
	 * Damier.
	 */
	private Damier chDamier;
	
	/**
	 * Nombre de lignes du damier.
	 */
	private int chLigne;
	
	/**
	 * Nombre de colonnes du damier.
	 */
	private int chColonne;
	

	/**
	 * Constructeur de PanelCentre.
	 * @param parDamier Un damier.
	 */
	public PanelCentre(Damier parDamier) {
		
		setOpaque(false);

		chDamier = parDamier;
		chLigne = chDamier.getNbLignes();
		chColonne = chDamier.getNbColonnes();
		chListeBouton = new BoutonCellule [chLigne][chColonne];
		setLayout(new  GridLayout(0, chColonne));
		
		for (int l=0; l < chLigne; l++) {
			for (int c=0; c < chColonne; c++) {
				chListeBouton[l][c] = new BoutonCellule(l,c, chDamier.getEtat(l, c));
				chListeBouton[l][c].setBorder(new LineBorder(Color.GRAY, 1));
				add(chListeBouton[l][c]);
			}
		}
		this.updatePanel();
	}		
	
	
	/**
	 * M�thode permettant de mettre � jour le jeu lors d'un changement de g�n�ration.
	 */
	public void updatePanel() {
		for (int l=0; l < chLigne; l++) {
			for (int c=0; c < chColonne; c++) {
				chListeBouton[l][c].setEtat(chDamier.getEtat(l, c));
				chListeBouton[l][c].setCouleur();
			}
		}
	}
	
	
	/**
	 * M�thode permettant de changer de damier (aleatoire, vide, ou exemple).
	 * Elle est utilis�e grace � la JMenuBar mise en place.
	 * @param parDamier Un damier.
	 */
	public void changementDamier(Damier parDamier) {
		
		chDamier = parDamier;
		
		removeAll();
		validate();
		
		chLigne = chDamier.getNbLignes();
		chColonne = chDamier.getNbColonnes();
		
		setLayout(new GridLayout(chLigne, chColonne));
		GridBagConstraints contrainte = new GridBagConstraints();
		contrainte.fill = GridBagConstraints.BOTH;
		
		chListeBouton = new BoutonCellule [chLigne][chColonne];
		for (int l=0; l < chLigne; l++) {
			for (int c=0; c < chLigne; c++) {
				BoutonCellule chBouton = new BoutonCellule(l, c, chDamier.getEtat(l, c));
				chBouton.setPreferredSize(new Dimension(15,15));
				chBouton.setCouleur();
				chBouton.setBorder(BorderFactory.createLineBorder(Color.GRAY));
				add(chBouton);
				chListeBouton[l][c] = chBouton;				
			}
		}
		revalidate();
		repaint();
	}
	
	
	/**
	 * M�thode permettant de mettre le PanelCentre � l'�coute du controleur.
	 * @param parControleur Le controleur.
	 */
	public void enregistreEcouteur (Controleur parControleur){
		for (int l=0; l < chLigne; l++)
			for (int c=0; c < chColonne; c++)
				chListeBouton[l][c].addActionListener(parControleur);
	}
	
}