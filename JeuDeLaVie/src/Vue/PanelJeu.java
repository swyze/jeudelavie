package Vue;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;

import Controleur.*;
import Modele.*;


/**
 * Cette classe regroupe les diff�rents Panels du jeu : PanelNord, PanelCentre, PanelSud.
 * Elle permet donc d'afficher tous les composants du jeu de la vie.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class PanelJeu extends JPanel {

	private Damier chDamier = new Damier();	
	private PanelSud chPanelSud;
	private PanelCentre chPanelCentre;
	private PanelNord chPanelNord;
	private ArrayList <JMenuItem> chListeJMenuItem;

	/**
	 * Constructeur de PanelJeu.
	 * @param parListeJMenuItem Liste des JMenuItem. Permet de les mettre � l'�coute du controleur.
	 */
	public PanelJeu(ArrayList <JMenuItem> parListeJMenuItem, JMenu parMenuExemples, ArrayList<String> parNomsDamiers,
			ArrayList <Damier> parListeDamiers) {
				
		setBackground(new Color(210, 251, 249));

		chListeJMenuItem = parListeJMenuItem;

		//***** Gestionnaire de r�partition *****//
		setLayout(new BorderLayout(10,10));
		
		//***** Instanciation des Panels *****//
		chPanelNord = new PanelNord(chDamier);
		chPanelCentre = new PanelCentre(chDamier);
		chPanelSud = new PanelSud();
		
		//***** AJOUTS *****//
		add(chPanelNord, BorderLayout.NORTH);
		add(chPanelCentre, BorderLayout.CENTER);
		add(chPanelSud, BorderLayout.SOUTH);


		//***** Instanciation du Controleur *****//
		Controleur chControleur = new Controleur(chDamier, chPanelNord, chPanelCentre, chPanelSud, this, 
				parNomsDamiers, parListeDamiers, parListeJMenuItem, parMenuExemples);
	}
	
	
	/**
	 * M�thode permettant de mettre le PanelCentre � l'�coute du controleur.
	 * @param parControleur Le controleur.
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		for (int i=0; i < chListeJMenuItem.size(); i++)
			chListeJMenuItem.get(i).addActionListener(parControleur);
	}

}