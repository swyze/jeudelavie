package Vue;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

import Controleur.Controleur;
import Modele.Data;


/**
 * Cette classe s'affiche au sud de PanelJeu, et donc de FenetreMere.
 * Elle permet d'interagir avec le jeu de la vie.
 * 
 * @author A. MARTIN T. ROGLIN
 */
public class PanelSud extends JPanel {
	
	private JButton chBoutons [] = new JButton [Data.BOUTONS_SUD.length];
	private JTextField chNomDamier = new JTextField(10);

	/**
	 * Constructeur de PanelSud. Permet l'instanciation des boutons qui permettent d'interagir avec le jeu de la vie.
	 */
	public PanelSud() {
	
		setOpaque(false);

		for (int i=0; i < Data.BOUTONS_SUD.length; i++){
			chBoutons[i] = new JButton(Data.BOUTONS_SUD[i]);
			chBoutons[i].setActionCommand(Data.BOUTONS_SUD[i]);
			chBoutons[i].setBackground(new Color(232, 232, 232));
			chBoutons[i].setMnemonic(Data.BOUTONS_SUD[i].charAt(0));

			add(chBoutons[i]);

			if (Data.BOUTONS_SUD[i] == Data.BOUTONS_SUD[2]) {
				JLabel chLabelNom = new JLabel(Data.LABEL_SUD);
				chLabelNom.setDisplayedMnemonic(Data.LABEL_SUD.charAt(0));
				add(chLabelNom);
				chNomDamier.setPreferredSize(new Dimension(10,27));
				chLabelNom.setLabelFor(chNomDamier);
				chNomDamier.setBackground(new Color(232, 232, 232));

				add(chNomDamier);

			}
		}
	}
	
	
	/**
	 * M�thode permettant de mettre le PanelCentre � l'�coute du controleur.
	 * @param parControleur Le controleur.
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		for (int i=0; i < chBoutons.length; i++)
			chBoutons[i].addActionListener(parControleur);
	}
	
	
	/**
	 * M�thode permettant de r�cup�rer le nom du damier entr� par l'utilisateur.
	 * @return String contenant le nom du damier.
	 */
	public String getNomNewDamier() {
		return chNomDamier.getText();
	}
	
	
	/**
	 * M�thode permettant de r�initialiser le champ de saisie du nom du damier.
	 */
	public void resetLabel() {
		chNomDamier.setText("");
	}
	
}